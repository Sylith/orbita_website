window.onload = function () {
  setTimeout(function () {
    document.getElementById("scrollpos").scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }, 500);
};
